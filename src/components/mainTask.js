import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import { deleteTask } from '../actions/postActions'

export class mainTask extends Component {

    handleDelete = (id) => {
        this.props.deleteTask(id)
    }

    render() {

        const {tasks} = this.props

        const taskList = tasks.length ?(
            tasks.map(task => {
                return (
                    <div className="card"  key={task.id}>
                        <div className="card-body">
                            <h5 className="card-title text-muted">{task.id+' - '+task.title}</h5>
                            <p className="card-text">{task.content}</p>
                            <Link to={'Tasks/view/'+task.id} className="card-link">View</Link>
                            <Link to="#" className="card-link" onClick={() => this.handleDelete(task.id)}>Delete</Link>
                        </div>
                    </div>
                )
            })
        ) : (
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title text-muted text-center">No todos left</h5>
                </div>
            </div>
        )


        return (
            <div>
               <center><h3>Task View</h3></center>
               {taskList}
            </div>
        )
    }
}

const mapDispatchProps = (dispatch) => {
    return{
        deleteTask: (id) => {dispatch(deleteTask(id))}
    }
}

export default connect(null, mapDispatchProps)(mainTask)
