import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

class Users extends Component{

    constructor(props) {
        super(props)
    
        this.state = {
             users: [],
             page: '',
        }

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount(){
        const {page} = this.state
       
        axios.get('https://reqres.in/api/users?page='+page)
        .then(response => {
            //console.log(response.data)
            this.setState({users: response.data.data}, () => 
                console.log(this.state));
            })
        .catch(error =>{
            //console.log(error)
        })
    }

    stringifyFormData(fd) {
        const data = {};
          for (let key of fd.keys()) {
            data[key] = fd.get(key);
        }
        return JSON.stringify(data, null, 2);
      }

    goToPage = e => {
        this.setState({page: e.target.value})
        
    }

    handleSubmit = e => { 
        e.preventDefault();
       console.log(e.target.elements.page_no.value)
        const data = new FormData(e.target);
        this.setState({
            res: this.stringifyFormData(data),
        });
        //console.log('Clicked')
        //console.log(data);
        console.log(this.state);

        this.componentDidMount()
      }
   
  
    render(){
        const {users,page} = this.state
        return(
            <div className="container">
                <br /><br /><br />
                <h3>User lists</h3>
                <form onSubmit={this.handleSubmit}>
                    <div className="input-group mb-3">
                        <input type="text" id="page_no" name="page_no" className="form-control" placeholder="Page number" value={page} onChange={this.goToPage}/>
                        <div className="input-group-append">
                            <button className="btn btn-outline-primary" >Go</button>
                        </div>
                    </div>
                </form>
                
                <div>Number of users: {users.length}</div>

                <table className="table">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Avatar</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        users.length ?
                        users.map(user =>                             
                            <tr key={user.id}>
                                <th scope="row">{user.id}</th>
                                <td>{user.last_name}</td>
                                <td>{user.first_name}</td>
                                <td>{user.email}</td>
                                <td><img src={user.avatar} height="50" alt="avatar"/></td>
                                <td><Link to={'Users/view/'+user.id} user_id={user.id}>View</Link></td>
                            </tr>
                            
                            
                        ):
                        <tr>
                            <td colSpan="5">
                                <div className="alert alert-warning alert-dismissible fade show" role="alert">
                                    <strong>No data</strong>
                                </div>
                            </td>
                        </tr>
                    }
                </tbody>
                </table>
                
            </div>
       )
    }
}

export default Users;