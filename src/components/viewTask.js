import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';

export class viewTask extends Component {
    render() {
        const task = this.props.tasks ? (
            <div className="container">
                <br /><br /><br />
                <h3>{this.props.tasks.title}</h3>
                <p>{this.props.tasks.content}</p>
            </div>
        ) : (
            <div className="text-center">No details</div>
        )
        return (
            <div className="container">
                {task}
                <Link to="/Tasks">Return to Tasks Page</Link>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    let id = ownProps.match.params.task_id;
    return {
        tasks: state.tasks.find((task) => {
            return task.id === id
        })
    }
}

export default connect(mapStateToProps)(viewTask)
