import React from 'react'
import { Link } from 'react-router-dom';

const Page404 = () =>{
    return (
        <div className="container text-center">
            <br /><br /><br />
            <h1>404 Page not found</h1>
            <h5>The location you are trying to access is not found.</h5>
            <br />
            <Link to="/">Return to Home Page</Link>
        </div>
    )
}

export default Page404
