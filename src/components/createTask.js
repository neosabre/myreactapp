import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addTask } from '../actions/postActions'

export class createTask extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            task_title: '',
            task_content: '',
        }

        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    setTask = e => {
        this.setState({ [e.target.name]: e.target.value });      
    }

    handleSubmit = (e) => { 
        e.preventDefault();

        let id = 1;

        if(this.props.tasks.length !== 0){
            id = this.props.tasks.slice(-1)[0].id+1
        }

        let task = {
            id: id,
            title: this.state.task_title,
            content: this.state.task_content
        };

        this.props.addTask(task)
        this.resetForm()

    }

    resetForm = () => {
        this.setState({
            task_title: '',
            task_content: '',
        })
      }


    render() {
        const {task_title,task_content} = this.state

        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <center><h3>Create Task</h3></center>

                    <div className="form-group">
                        <label htmlFor="task_title">Title</label>
                        <input type="text"  id="task_title" name="task_title" className="form-control" placeholder="Your title here..." value={task_title} onChange={this.setTask} />
                        <label htmlFor="task_content">Content</label>
                        <textarea id="task_content" name="task_content" className="form-control" placeholder="Your content here..." rows="7" value={task_content} onChange={this.setTask} />
                        <small id="taskHelp" className="form-text text-muted">* Input any text and I will create a card for your tasks.</small>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
       
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        tasks: state.tasks
    }
}

const mapDispatchProps = (dispatch) => {
    return{
        addTask: (task) => {dispatch(addTask(task))}
    }
}

export default connect(mapStateToProps,mapDispatchProps)(createTask)
