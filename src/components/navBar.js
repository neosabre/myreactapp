import React from 'react'
import { NavLink, Link } from 'react-router-dom'
import ReactIcon from '../images/logo192.png'

const navBar = () => {
    return (
        <div>
            <nav className="navbar navbar-expand-sm fixed-top navbar-dark bg-dark">
            <Link className="navbar-brand" to="#"><img src={ReactIcon} alt="Logo" height="30"/></Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" >
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/">Home</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/Tasks">Tasks</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/Users">Users</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/About">About</NavLink>
                    </li>
                </ul>
            </div>
            </nav>
        </div>
    )
}

export default navBar
