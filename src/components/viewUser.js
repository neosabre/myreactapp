import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import axios from 'axios';

export class viewUser extends Component {


    constructor(props) {
        super(props)
    
        this.state = {
             id: '',
             users: []
        }

    }
    
    componentDidMount(){
        //console.log(this.props)
        const user_id = this.props.match.params.user_id
       
        axios.get('https://reqres.in/api/users/'+user_id)
        .then(response => {
            //console.log(response.data)
            this.setState({users: response.data.data}, () => 
                console.log(this.state));
            })
        .catch(error =>{
            //console.log(error)
        })
    }
    
    render() {
        const {users} = this.state
        const user = this.state.users ? (
            <div className="container text-center">
                <br /><br /><br />
                <img src={users.avatar} height="100" alt="avatar"/>
                <h3>{users.first_name+' '+users.last_name}</h3>
                <p>{users.email}</p>
            </div>
        ) : (
            <div className="text-center">No details</div>
        )
        return (
            <div className="container">
                {user}
                <Link to="/Users">Return to Users Page</Link>
            </div>
        )
    }
}

export default viewUser
