import React, { Component } from 'react'
import CreateTask from './createTask'
import MainTask from './mainTask'
import { connect } from 'react-redux'

class Tasks extends Component {

    render() {
        return (
            <div className="container">
                <br /><br /><br />
                <h3>Tasks</h3>
                <div className="row">
                    <div className="col-sm-4">
                        <CreateTask/>
                    </div>
                    <div className="col-sm-8">
                       <MainTask tasks={this.props.tasks} />
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        tasks: state.tasks
    }
}

export default connect(mapStateToProps)(Tasks)
