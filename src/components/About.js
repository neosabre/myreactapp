import React from 'react'

const About = () =>{
    return (
        <div className="container">
            <br /><br /><br />
            <h3>About</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque id orci gravida scelerisque nec ut leo. Vestibulum dolor sem, gravida sit amet rhoncus a, rhoncus ut ipsum. Nunc a ligula ex. Nulla sapien metus, feugiat id sapien sit amet, tempor mollis elit. Morbi sodales, sapien quis tincidunt accumsan, quam quam dictum risus, sed volutpat ex nibh quis lorem. Proin placerat rhoncus dictum. Duis in ex a sapien placerat semper. Sed non augue id orci ultrices eleifend. Vestibulum vestibulum lectus id malesuada laoreet. Fusce libero turpis, cursus at justo ut, vulputate lacinia lacus. Proin eu dui eleifend, tempor urna a, vestibulum libero. Curabitur facilisis nunc non ipsum elementum tristique. Praesent sit amet eros leo. Morbi viverra nulla sit amet eros varius accumsan. Pellentesque iaculis varius tempor. Phasellus ut augue enim. </p>
        </div>
    )
}

export default About
