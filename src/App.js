import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Navbar from './components/navBar'
import Page404 from './components/Page404'
import Home from './components/Home'
import About from './components/About'
import Tasks from './components/Tasks'
import viewTask from './components/viewTask'
import Users from './components/Users'
import viewUser from './components/viewUser'

class App extends Component {
    render() {
        return(
            <BrowserRouter>
                <div className="App  mainTask">
                    <Navbar />
                    <Switch>
                        <Route exact path='/' component={Home} />
                        <Route exact path='/About' component={About} />
                        <Route exact path='/Tasks' component={Tasks} />
                        <Route exact path='/Tasks/view/:task_id' component={viewTask} />
                        <Route exact path='/Users' component={Users} />
                        <Route exact path='/Users/view/:user_id' component={viewUser} />
                        <Route component={Page404} />
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;