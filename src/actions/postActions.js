export const deleteTask = (id) => {
    return {
        type: 'delete_task',
        id: id
    }
}

export const addTask = (task) => {
    return {
        type: 'add_task',
        task: task
    }
}