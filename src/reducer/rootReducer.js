const initState = {
    tasks: [
        {id: 1, title: 'Buy some bread', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque id orci gravida scelerisque nec ut leo. Vestibulum dolor sem, gravida sit amet rhoncus a, rhoncus ut ipsum. Nunc a ligula ex. Nulla sapien metus, feugiat id sapien sit amet, tempor mollis elit. Morbi sodales, sapien quis tincidunt accumsan, quam quam dictum risus, sed volutpat ex nibh quis lorem. Proin placerat rhoncus dictum. Duis in ex a sapien placerat semper. Sed non augue id orci ultrices eleifend. Vestibulum vestibulum lectus id malesuada laoreet. Fusce libero turpis, cursus at justo ut, vulputate lacinia lacus. Proin eu dui eleifend, tempor urna a, vestibulum libero. Curabitur facilisis nunc non ipsum elementum tristique. Praesent sit amet eros leo. Morbi viverra nulla sit amet eros varius accumsan. Pellentesque iaculis varius tempor. Phasellus ut augue enim. '},
        {id: 2, title: 'Get some bread', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque id orci gravida scelerisque nec ut leo. Vestibulum dolor sem, gravida sit amet rhoncus a, rhoncus ut ipsum. Nunc a ligula ex. Nulla sapien metus, feugiat id sapien sit amet, tempor mollis elit. Morbi sodales, sapien quis tincidunt accumsan, quam quam dictum risus, sed volutpat ex nibh quis lorem. Proin placerat rhoncus dictum. Duis in ex a sapien placerat semper. Sed non augue id orci ultrices eleifend. Vestibulum vestibulum lectus id malesuada laoreet. Fusce libero turpis, cursus at justo ut, vulputate lacinia lacus. Proin eu dui eleifend, tempor urna a, vestibulum libero. Curabitur facilisis nunc non ipsum elementum tristique. Praesent sit amet eros leo. Morbi viverra nulla sit amet eros varius accumsan. Pellentesque iaculis varius tempor. Phasellus ut augue enim. '},
        {id: 3, title: 'Eat some bread', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque id orci gravida scelerisque nec ut leo. Vestibulum dolor sem, gravida sit amet rhoncus a, rhoncus ut ipsum. Nunc a ligula ex. Nulla sapien metus, feugiat id sapien sit amet, tempor mollis elit. Morbi sodales, sapien quis tincidunt accumsan, quam quam dictum risus, sed volutpat ex nibh quis lorem. Proin placerat rhoncus dictum. Duis in ex a sapien placerat semper. Sed non augue id orci ultrices eleifend. Vestibulum vestibulum lectus id malesuada laoreet. Fusce libero turpis, cursus at justo ut, vulputate lacinia lacus. Proin eu dui eleifend, tempor urna a, vestibulum libero. Curabitur facilisis nunc non ipsum elementum tristique. Praesent sit amet eros leo. Morbi viverra nulla sit amet eros varius accumsan. Pellentesque iaculis varius tempor. Phasellus ut augue enim. '},
        {id: 4, title: 'Give some bread', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque id orci gravida scelerisque nec ut leo. Vestibulum dolor sem, gravida sit amet rhoncus a, rhoncus ut ipsum. Nunc a ligula ex. Nulla sapien metus, feugiat id sapien sit amet, tempor mollis elit. Morbi sodales, sapien quis tincidunt accumsan, quam quam dictum risus, sed volutpat ex nibh quis lorem. Proin placerat rhoncus dictum. Duis in ex a sapien placerat semper. Sed non augue id orci ultrices eleifend. Vestibulum vestibulum lectus id malesuada laoreet. Fusce libero turpis, cursus at justo ut, vulputate lacinia lacus. Proin eu dui eleifend, tempor urna a, vestibulum libero. Curabitur facilisis nunc non ipsum elementum tristique. Praesent sit amet eros leo. Morbi viverra nulla sit amet eros varius accumsan. Pellentesque iaculis varius tempor. Phasellus ut augue enim. '},
        {id: 5, title: 'Throw some bread', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et neque id orci gravida scelerisque nec ut leo. Vestibulum dolor sem, gravida sit amet rhoncus a, rhoncus ut ipsum. Nunc a ligula ex. Nulla sapien metus, feugiat id sapien sit amet, tempor mollis elit. Morbi sodales, sapien quis tincidunt accumsan, quam quam dictum risus, sed volutpat ex nibh quis lorem. Proin placerat rhoncus dictum. Duis in ex a sapien placerat semper. Sed non augue id orci ultrices eleifend. Vestibulum vestibulum lectus id malesuada laoreet. Fusce libero turpis, cursus at justo ut, vulputate lacinia lacus. Proin eu dui eleifend, tempor urna a, vestibulum libero. Curabitur facilisis nunc non ipsum elementum tristique. Praesent sit amet eros leo. Morbi viverra nulla sit amet eros varius accumsan. Pellentesque iaculis varius tempor. Phasellus ut augue enim. '}
    ]

}

const rootReducer = (state = initState, data) => {
    if(data.type === 'delete_task'){
        let newTasks = state.tasks.filter(tasks => {
            return data.id !== tasks.id
        });

        return {
            ...state,
            tasks: newTasks
            
        }
    }
    else if(data.type === 'add_task'){
        return {
            tasks: [...state.tasks,data.task]
        }
    }

    return state;
}

export default rootReducer